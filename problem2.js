function findLastCar(inventory) {
    let lastCar_data = {};
    console.log(`Last car is a ${inventory[inventory.length - 1].car_year} ${inventory[inventory.length - 1].car_make} ${inventory[inventory.length - 1].car_model}`)
    lastCar_data["id"] = 33
    lastCar_data["car_make"] = inventory[inventory.length - 1].car_make
    lastCar_data["car_model"] = inventory[inventory.length - 1].car_model
    lastCar_data["car_year"] = inventory[inventory.length - 1].car_year
    return lastCar_data;
}
module.exports = findLastCar;