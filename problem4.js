function printCarYears(inventory) {
    let carYearsData = [];
    for (let i =0 ; i < inventory.length; i++) {
        carYearsData.push(inventory[i].car_year);
    }
    console.log(carYearsData)
    return carYearsData;
}

module.exports = printCarYears;