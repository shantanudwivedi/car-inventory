function findOlderCars(carYearData) {
    let count = 0;
    let olderCars = []
    for (let i = 0; i < carYearData.length; i++) {
        if(carYearData[i] > 2000) {
            count += 1;
        }
        else {
            olderCars.push(carYearData[i])
        }
    }
    console.log(`${count} cars were made before the year 2000`)
    console.log(olderCars.length)
    return olderCars;
}

module.exports = findOlderCars