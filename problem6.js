function findBMWAndAUDICars(inventory) {
    let bmwAndAudiCars = [];
    let carsObj = {}
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_make === "BMW" || inventory[i].car_make === "Audi") {
            carsObj["id"] = inventory[i].id
            carsObj["car_make"] = inventory[i].car_make
            carsObj["car_model"] = inventory[i].car_model
            carsObj["car_year"] = inventory[i].car_year
        }
        bmwAndAudiCars.push(carsObj);
    }
    console.log(bmwAndAudiCars);
    return bmwAndAudiCars;
}

module.exports = findBMWAndAUDICars;