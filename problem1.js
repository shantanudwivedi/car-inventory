function findCar(inventory, searchId) {
    let car_data = {};
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id === searchId) {
            console.log(`Car ${searchId} is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`)
            car_data["id"] = searchId
            car_data["car_make"] = inventory[i].car_make
            car_data["car_model"] = inventory[i].car_model
            car_data["car_year"] = inventory[i].car_year
            break;
        }
    }
    return car_data
}

module.exports = findCar;