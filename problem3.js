function printCarModelsAplhabetically(inventory) {
    let carModelsData = [];
    for (let i =0 ; i < inventory.length; i++) {
        carModelsData.push(inventory[i].car_model);
    }
    carModelsData.sort();
    return carModelsData;
}

module.exports = printCarModelsAplhabetically;